import graphene
from .models import Movie, Person


class MovieType(graphene.ObjectType):
    released = graphene.Int()
    title = graphene.String()
    tagline = graphene.String()

    def resolve_released(parent, info):
        return parent.released

    def resolve_title(parent, info):
        return parent.title

    def resolve_tagline(parent, info):
        return parent.tagline


class ActedInType(graphene.ObjectType):
    roles = graphene.List(graphene.String)

    def resolve_roles(parent, info):
        return parent.roles


class ReviewType(graphene.ObjectType):
    rating = graphene.Int()
    summary = graphene.String()

    def resolve_rating(parent, info):
        return parent.rating

    def resolve_summary(parent, info):
        return parent.summary


class PersonType(graphene.ObjectType):
    born = graphene.Int()
    name = graphene.String()
    movies_acted_in = graphene.List(MovieType)
    movies_written = graphene.List(MovieType)
    movies_produced = graphene.List(MovieType)
    movies_directed = graphene.List(MovieType)
    movies_reviewed = graphene.List(MovieType)
    reviews_written = graphene.List(ReviewType)
    roles_played = graphene.List(ActedInType)

    def resolve_born(parent, info):
        return parent.born

    def resolve_name(parent, info):
        return parent.name

    def resolve_movies_acted_in(parent, info):
        return parent.movies_acted_in

    def resolve_movies_written(parent, info):
        return parent.movies_written

    def resolve_movies_produced(parent, info):
        return parent.movies_produced

    def resolve_movies_directed(parent, info):
        return parent.movies_directed

    def resolve_movies_reviewed(parent, info):
        return parent.movies_reviewed

    def resolve_reviews_written(parent, info):
        return [m.reviewers.relationship(parent) for m in parent.movies_reviewed]

    def resolve_roles_played(parent, info):
        return [m.actors.relationship(parent) for m in parent.movies_acted_in]

class CreateMovie(graphene.Mutation):
    class Arguments:
        released = graphene.Int(required=True)
        title = graphene.String(required=True)
        tagline = graphene.String(required=True)

    success = graphene.Boolean()
    movie = graphene.Field(lambda: MovieType)

    def mutate(self, info, **kwargs):
        movie = Movie(**kwargs)
        movie.save()

        return CreateMovie(movie=movie, success=True)

class CreatePerson(graphene.Mutation):
    class Arguments:
        born = graphene.Int()
        name = graphene.String()


    success = graphene.Boolean()
    person = graphene.Field(lambda: PersonType)

    def mutate(self, info, **kwargs):
        person = Person(**kwargs)
        person.save()

        return CreatePerson(person=person, success=True)


class Mutations(graphene.ObjectType):
    create_movie = CreateMovie.Field()
    create_person = CreatePerson.Field()

class Query(graphene.ObjectType):
    all_movies = graphene.List(MovieType)
    person_by_name = graphene.Field(PersonType, name=graphene.String(required=True))
    person_by_year = graphene.Field(PersonType, year=graphene.Int(required=True))


    def resolve_all_movies(self, info):
        return Movie.nodes.all()

    def resolve_person_by_name(self, info, name):
        try:
            return Person.nodes.get(name=name)
        except Person.DoesNotExist:
            return None

    def resolve_person_by_born(self, info, year):
        try:
            return Person.nodes.get(born=year)
        except Person.DoesNotExist:
            return None


schema = graphene.Schema(query=Query, mutation=Mutations)