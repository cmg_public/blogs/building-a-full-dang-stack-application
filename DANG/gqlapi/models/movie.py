# gqlapi/models/movie.py
from neomodel import (
    StringProperty,
    IntegerProperty,
    ArrayProperty,
    StructuredNode,
    StructuredRel,
    RelationshipFrom,
)

class ActedIn(StructuredRel):
    roles = ArrayProperty(StringProperty())


class Reviewed(StructuredRel):
    rating = IntegerProperty()
    summary = StringProperty()


class Movie(StructuredNode):
    released = IntegerProperty()
    tagline = StringProperty()
    title = StringProperty()

    actors = RelationshipFrom('.person.Person', 'ACTED_IN', model=ActedIn)
    writers = RelationshipFrom('.person.Person', 'WROTE')
    producers = RelationshipFrom('.person.Person', 'PRODUCED')
    directors = RelationshipFrom('.person.Person', 'DIRECTED')
    reviewers = RelationshipFrom('.person.Person', 'REVIEWED', model=Reviewed)
