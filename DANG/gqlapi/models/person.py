from neomodel import (
    StringProperty,
    IntegerProperty,
    StructuredNode,
    RelationshipTo,
    RelationshipFrom,
)


class Person(StructuredNode):
    name = StringProperty()
    born = IntegerProperty()

    movies_acted_in = RelationshipTo('.movie.Movie', 'ACTED_IN')
    movies_written = RelationshipTo('.movie.Movie', 'WROTE')
    movies_produced = RelationshipTo('.movie.Movie', 'PRODUCED')
    movies_directed = RelationshipTo('.movie.Movie', 'DIRECTED')
    movies_reviewed = RelationshipTo('.movie.Movie', 'REVIEWED')
    followers = RelationshipFrom('Person', 'FOLLOWS')
    following = RelationshipTo('Person', 'FOLLOWS')
