from django.apps import AppConfig


class GqlapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gqlapi'
